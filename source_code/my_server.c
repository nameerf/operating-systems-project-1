/*
* Sto arxeio ayto ylopoioume ton server. H leitourgia tou einai h ekshs:
* Desmeuei ena tmhma (2048 bytes) ths mnhmhs sto opoio exoun prosbash
* oles oi diergasies-paidia pou dhmiourgei, dhladh dhmiourgei koinh mnhmh.
* Sto tmhma ayto ths koinhs mnhmhs apo8hkeuontai oles oi plhrofories gia
* ka8e logariamo pou dhmiourgeitai. Molis ksekinhsei o server, dexetai klhseis
* apo clients, syndeetai me aytous meso sockets kai gia ka8e aithsh pou lambanei
* apo kapoion client dhmiourgei, meso ths synarthshs systhmatos fork(), mia nea
* diergasia-paidi h opoia eksyphretei ton ekastote client kai termatizei. Epishs
* stelnetai kai ena "apanthtiko" mhnyma piso ston ekastote client, meso ths synarthshs
* write(), pou eite periexei apla ena enhmerotiko mhnyma eite plhrofories pou zhthse
* o ekastote client. H eksodos apo ton server ginetai me ton syndyasmo ton plhktron
* Ctrl+C. Telos na shmeio8ei oti gia ta sxolia isxuei oti kai ston client
* dhladh gia entoles pou xrhsimopoiountai, idies polles fores, ta sxolia
* gia thn ka8e entolh yparxoun mono sthn proth emfanish ths entolhs.
*/

#include <sys/ipc.h>		/* gia thn koinh mnhmh. */
#include <sys/shm.h>		/* gia thn koinh mnhmh. */
#include <semaphore.h>		/* gia tous shmaforous. */
#include <fcntl.h>			/* gia tous shmaforous. */
#include <sys/stat.h>		/* gia tous shmaforous. */
#include <signal.h>			/* gia ta signals. */

#include "types.h"

#define SHM_KEY 2615		/* to "kleidi" ths koinhs mnhmhs. */
#define SHM_SIZE 2048		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SEM_NAME "2615"		/* to onoma tou shmaforou. */

/* dhloseis synarthseon. */
void exit_server(int);
void sig_child(int);
void action(account *, account *);
void save_last4transactions(account *, account *, int);
void export_last4transactions(account *);

/* tis parakato metablhtes tis dhlonoume global giati
 * xrhsimopoiountai sthn synarthsh exit_server(). */
int shm_id, error; //metablhtes gia thn koinh mnhmh.
sem_t *my_sem; //metablhth shmaforou

/* enas char pinakas o opoios xrhsimopoieitai stis synarthseis read() kai write()
 * gia thn anagnosh mhnumaton apo tous clients kai thn eggrafh mhnymaton piso se aytous. */
char buffer[300];

/* Synarthsh eksodou apo ton server. Ekteleitai molis "piasei" to shma Ctrl-C. */
void exit_server(int sig) {
	printf("\nExiting server...\n");
	signal(SIGINT, SIG_DFL);

	sem_close(my_sem); //kleisimo shmaforou.
	sem_unlink(SEM_NAME); //diagrafh shmaforou.
	shmctl(shm_id, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
	exit(0);
}

/* Synarthsh h opoia apotrepei thn dhmiourgia ton legomenon "zombie" diergasion. */
void sig_child(int sig) {
	pid_t pid;
	int stat;

	while((pid = waitpid(-1, &stat, WNOHANG)) > 0)
		printf("Child %d terminated.\n", pid);
}

int main(void)
{
	int sd1, sd2, client_length, len; //metablhtes gia ta sockets.
	struct sockaddr_un local, remote; //domes gia tis die8unseis ton socket tou client kai tou server.
	pid_t child_pid; //metablhth typou pid_t gia thn apo8hkeush tou pid ka8e dhergasias-paidiou.
	account *data; //deikths typoy account pou deixnei sthn proth 8esh ths koinhs mnhmhs.

	signal(SIGINT, exit_server); //apeleu8eronei tous "porous" kai termatizei ton server.
	signal(SIGCHLD, sig_child); //apotrepei thn dhmiourgia ton zombie diergasion.

	my_sem = sem_open(SEM_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, 1); //dhmiourgia-anoigma shmaforou.
	if(my_sem == SEM_FAILED) {
		printf("Could not open semaphore!\n" );
		perror(" " );
		exit(1) ;
	}

	shm_id = shmget(SHM_KEY, SHM_SIZE, 0600 | IPC_CREAT); //dhmiourgia koinhs mnhmhs.
	if(shm_id < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	/* proskollhsh diergsias sthn koinh mnhmh. Isxuei kai gia tis diergasies-paidia,
	 * afou klhronomoun apo th diergasia-patera. */
	data = shmat(shm_id, NULL, 0);
	if(data == (account *)-1) {
		printf("Could not attach to shared memory!\n");
		exit(1);
	}

	/* dhmiourgoume enan kainourgio logariasmo me kena stoixeia kai ton apo8hkeuoume
	 * sthn proth 8esh ths koinhs mnhmhs. ayto to kanoume gia na xrhsimopoioume th metablhth
	 * id os counter gia na metrame posoi logariasmoi exoun apo8hkeutei sthn koinh mnhmh
	 * kai na exoume eukolh prosbash se aytous. */
	account *first_account = (account*)malloc(sizeof(account));
	first_account->id = 0; //arxika exoume 0 apo8hkeumenous logariasmous.
	*data = *first_account; //antigrafei ta periexomena tou first_account sthn proth 8esh ths koinhs mnhmhs.

	if((sd1 = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) { //dhmiourgia tou socket tou server.
		perror("socket");
		exit(1);
	}

	local.sun_family = AF_UNIX;
	strcpy(local.sun_path, SOCK_PATH);
	unlink(local.sun_path); //diagrafh opoion prohgoumenon sockets me to idio onoma pou exoume orisei.
	len = strlen(local.sun_path) + sizeof(local.sun_family);
	/* syndesh tou socket descriptor tou client me ena local port tou L.S. */
	if(bind(sd1, (struct sockaddr *)&local, len) == -1) {
		perror("bind");
		exit(1);
	}

	if(listen(sd1, 5) == -1) { //"akouei" gia kainourgies sundeseis kai tis topo8etei se mia oura.
		perror("listen");
		exit(1);
	}

	/* atermon broxos. sundeetai me clients pou exoun kanei aithsh gia eksyphrethsh,
	 * diabazei ta dedomena pou exoun steilei meso ths synarthshs read, kai gia ka8e
	 * client, dhmiourgei meso ths fork() mia kainourgia diergasia-paidi h opoia
	 * eksyphretei ton ekastote client kai termatizei. */
	for(;;) {
		int n;
		printf("Waiting for connection...\n");
		client_length = sizeof(remote);
		if((sd2 = accept(sd1, (struct sockaddr *)&remote, &client_length)) == -1) { //pragmatopoihsh syndeshs me client.
			perror("accept");
			exit(1);
		}
		printf("Connected.\n");

		/* diabazei to "mhnyma", apo8hkeuontas to ston pinaka buffer, me ta dedomena pou esteile o client. */
		n = read(sd2, buffer, 300);
		if(n <= 0) {
			if(n < 0) perror("read");
			exit(1);
		}

		child_pid = fork(); //dhmiourgia mias neas diergasias-paidi.

		if(child_pid == 0) { //h diergasia-paidi "ekteleitai" edo.
			close(sd1); //kleisimo tou socket tou server.

			sem_wait(my_sem); //kleidoma shmaforou.
			action(data, (account*)buffer);	//ergasies sthn koinh mnhmh.
			sem_post(my_sem); //apeleu8erosh shmaforou.

			/* apanthtiko mhnyma pros ton ekastote client, pou eite periexei apla
			 * ena enhmerotiko mhnyma eite plhrofories pou zhthse o client. */
			write(sd2, buffer, sizeof(buffer));

			error = shmdt(data); //apokollhsh diergasias apo thn koinh mnhmh.
			if(error == -1) {
				printf("Could not detach from shared memory!\n");
				exit(1);
			}
			exit(0); //termatismos diergasias-paidiou.
		}
		close(sd2); //kleisimo tou sindemenou socket apo thn diergasia-patera.
	}

	return 0;
}

/*
* Synarthsh gia thn pragmatopoihsh ton ekshs energeion: apo8hkeush neou logariasmou
* sthn koinh mnhmh, kata8esh xrhmaton se yparxon logariamo sthn koinh mnhmh, analhpsh
* xrhmaton apo yparxon logariasmo sthn koinh mnhmh kai telos enhmerosh tou bibliariou
* enos yparxontos logariasmou. Dexetai os orismata dyo deiktes typou account. O protos
* deixnei sthn proth 8esh ths koinhs mnhmhs opou exoume apo8hkeuesei enan "adeio" logariasmo
* me skopo na metrame tous logariasmous pou apo8hkeuoume kai na exoume eukolh prosbash se aytous
* kai o deuteros deikths deixnei sth domh account pou mas esteile o client mazi me thn energeia
* pou 8elei na ektelesoume (o server dhladh). An o logariasmos den yparxei sthn koinh mnhmh
* epistrefetai antistoixo mhnyma ston client.
*/
void action(account *sm_data, account *new_account)
{
	switch(new_account->action) {
		case CREATE_ACCOUNT: { //apo8hkeush tou neodhmiourgh8entos logariamsou, pou esteile o client, sthn koinh mnhmh.
			/* elegxoume an exoume xoro sthn koinh mnhmh na apo8hkeusoume ton kainourgio logariasmo
			 * allios den ton apo8hkeuoume kai apo8hkeuoume ston buffer katallhlo mhnyma gia na steiloume
			 * piso ston client enhmeronontas ton. */
			if(sizeof(account) * (sm_data->id+1) > SHM_SIZE) {
				strcpy(buffer, "Account hasn't been created because shared memory is full!\n");
				return;
			}
			sm_data->id++; //ayksanei ton ari8mo ton logariasmon pou exoun apo8hkeutei sthn koinh mnhmh kata 1.
			int temp = sm_data->id;
			sm_data = sm_data + sm_data->id; //phgainei ston epomeno logariasmo sthn koinh mnhmh.
			new_account->id = temp;
			*sm_data = *new_account; //antigrafei ta periexomena toy logariasmou pou "hr8e" sthn epomenh kenh 8esh ths koinhs mnhmhs.
			strcpy(buffer, "Account created successfully!\n"); //apo8hkeush ston buffer katallhlou mhnymatos gia na stal8ei piso ston client.
		} break;
		case DEPOSIT: { //kata8esh xrhmaton se yparxon logariasmo.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto
				/* apo8hkeuoyme sthn total_accounts ton synoliko ari8mo ton logariasmon pou einai apo8hkeuemenoi sthn koinh mnhmh. */
				int total_accounts = sm_data->id;
				int i;
				/* "diabazoume" seiriaka tous apo8hkeumenous sthn koinh mnhmh logariasmous kai
				 * elegxoume an kapoios apo aytous einai "idios" (idio onoma kai epi8eto) me ton logarismo
				 * pou esteile o client. */
				for(i=1; i<=total_accounts; i++) {
					sm_data++; //phgainei ston epomeno logariasmo sthn koinh mnhmh.
					/* an to onoma kai to epomymo pou edose o xrhsths yparxoun se logariasmo sthn koinh mnhmh
					 * kane thn kata8esh enhmeronontas ton logariasmo. */
					if((strcmp(new_account->name, sm_data->name) == 0) && (strcmp(new_account->surname, sm_data->surname) == 0)) {
						/* apo8hkeuei thn sygkekrimenh synallagh stis 4 teleutaies. */
						save_last4transactions(sm_data, new_account, DEPOSIT);
						sm_data->balance += new_account->amount; //enhmerosnei to upoloipo tou logariasmou.
						sm_data->all_transactions++; //auksanei tis synallages kata 1.
						strcpy(buffer, "The deposit completed successfully!\n");
						break;
					}
					strcpy(buffer, "Account not found!\n"); //o logariasmos den bre8hke sthn koinh mnhmh. steile analogo mhnyma.
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto
				if(new_account->id <= sm_data->id) { //an to id tou logariasmou pou psaxnoume uparxei sthn koinh mnhmh kanoume ta idia opos pano.
					save_last4transactions(sm_data+(new_account->id), new_account, DEPOSIT);
					(sm_data+(new_account->id))->balance += new_account->amount;
					(sm_data+(new_account->id))->all_transactions++;
					strcpy(buffer, "The deposit completed successfully!\n");
				}
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
		case WITHDRAW: { //analhpsh xrhmaton apo yparxon logariasmo.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto.
				int total_accounts = sm_data->id;
				int i;
				for(i=1; i<=total_accounts; i++) {
					sm_data++;
					if((strcmp(new_account->name, sm_data->name) == 0) && (strcmp(new_account->surname, sm_data->surname) == 0)) {
						/* an to poso pou zhth8hke gia anlhpsh einai megalutero tou trexontos ypoloipou
						 * akurose thn synallagh kai apo8hkeuse ston buffer katallhlo mhnyma gia na stal8ei
						 * piso ston client enhmeronontas ton. */
						if(sm_data->balance - new_account->amount < 0)
							strcpy(buffer, "Transaction canceled!\nNot enough money in the account!\n");
						else { //allios sunexise me thn analhpsh.
							save_last4transactions(sm_data, new_account, WITHDRAW);
							sm_data->balance -= new_account->amount;
							sm_data->all_transactions++;
							strcpy(buffer, "The withdrawal completed successfully!\n");
						}
						break;
					}
					strcpy(buffer, "Account not found!\n");
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto.
				if(new_account->id <= sm_data->id) {
					if((sm_data+(new_account->id))->balance - new_account->amount < 0)
						strcpy(buffer, "Transaction canceled!\nNot enough money in the account!\n");
					else {
						save_last4transactions(sm_data+(new_account->id), new_account, WITHDRAW);
						(sm_data+(new_account->id))->balance -= new_account->amount;
						(sm_data+(new_account->id))->all_transactions++;
						strcpy(buffer, "The withdrawal completed successfully!\n");
					}
				}
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
		case UPDATE_ACCOUNT: { //enhmerosh bibliariou/logariasmou.
			if(new_account->id == UNKNOWN) { //psaxnoume me to onoma kai to epi8eto.
				int total_accounts = sm_data->id;
				int i;
				for(i=1; i<=total_accounts; i++) {
					sm_data++;
					if((strcmp(new_account->name, sm_data->name) == 0) && (strcmp(new_account->surname, sm_data->surname) == 0)) {
						/* afou o logariasmos bre8hke dhmiourghse ena string me tis 4 teleutaies synallages
						 * gia na stal8ei piso ston client. */
						export_last4transactions(sm_data);
						break;
					}
					strcpy(buffer, "Account not found!\n");
				}
			}
			else { //exoume valid id opote psaxnoume me bash ayto.
				if(new_account->id <= sm_data->id)
					export_last4transactions((sm_data+(new_account->id)));
				else
					strcpy(buffer, "Account not found!\n");
			}
		} break;
	}
}

/*
* Synarthsh h opoia apo8hkeuei thn teleutaia synallagh se mia domh (blepe types.h)
* h opoia krataei tis 4 teleutaies synallages gia ka8e logariasmo. Dexetai os
* orismata dyo deiktes se domh typou account kai enan int me ton typo ths synallaghs
* kata8esh h analhpsh.
*/
void save_last4transactions(account *acc, account *new_acc, int action)
{
	int index = acc->all_transactions;
	/* an oi mexri tora synallages einai ligoteres apo 4 apo8hkeuse tis
	 * synallages pou erxontai stis 8eseis 0,1,2,3. */
	if(index < 4) {
		acc->last4transactions[index].type = action;
		acc->last4transactions[index].balance_before = acc->balance;
		acc->last4transactions[index].amount = new_acc->amount;
	}
	/* allios kane ena shifting stis yparxouses synallages, dhladh apo8hkeuse
	 * thn 2h sthn 1h, thn 3h sthn 2h, thn 4h sthn 3h kai thn teleutaia
	 * synallagh pou hr8e sthn 4h. */
	else {
		acc->last4transactions[0] = acc->last4transactions[1];
		acc->last4transactions[1] = acc->last4transactions[2];
		acc->last4transactions[2] = acc->last4transactions[3];
		/* teleutaia (4h) synallagh. */
		acc->last4transactions[3].type = action;
		acc->last4transactions[3].balance_before = acc->balance;
		acc->last4transactions[3].amount = new_acc->amount;
	}
}

/*
* Synarthsh h opoia "ftiaxnei" meso synenoseon ena megalo string (pinaka me chars) to
* opoio periexei tis plhrofories sxetika me tis 4 teleutaies synallages tou logariasmou
* kai to apo8hkeuei ston pinaka buffer gia na stal8ei piso ston client. H synarthsh
* ayth kaleitai mono otan o client zhthsei enhmerosei bibliariou/logariasmou. Pairnei
* os orisma enan deikth pros th domh account tou pros enhmerosh logariasmou kai den
* epistrefei tipota.
*/
void export_last4transactions(account *acc)
{
	char account_info[200];
	char tmp[20];
	char str[] = "Balance before | Type | Amount | New balance\n";

	strcat(account_info, str);

	int i;
	for(i=0; i<4; i++) {
		if(acc->last4transactions[i].type == UNKNOWN)
			continue;
		sprintf(tmp, "%d |", acc->last4transactions[i].balance_before);
		strcat(account_info, tmp);
		int type = acc->last4transactions[i].type;
		if(type == DEPOSIT) {
			strcat(account_info, " deposit |");
			sprintf(tmp, " %d |", acc->last4transactions[i].amount);
			strcat(account_info, tmp);
			sprintf(tmp, " %d\n", acc->last4transactions[i].balance_before + acc->last4transactions[i].amount);
			strcat(account_info, tmp);
		}
		else if(type == WITHDRAW) {
			strcat(account_info, " withdraw |");
			sprintf(tmp, " %d |", acc->last4transactions[i].amount);
			strcat(account_info, tmp);
			sprintf(tmp, " %d\n", acc->last4transactions[i].balance_before - acc->last4transactions[i].amount);
			strcat(account_info, tmp);
		}
	}

	strcpy(buffer, account_info);
}
